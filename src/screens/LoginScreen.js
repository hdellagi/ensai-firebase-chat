import React, { useContext, useState } from 'react';
import { StyleSheet, View, Image } from 'react-native';
import { Title } from 'react-native-paper';
import FormButton from '../components/FormButton';
import FormInput from '../components/FormInput';
import Loading from '../components/Loading';
import { AuthContext } from '../navigation/loginGestion';



export default function SeConnecterScreen({ navigation }) {
  const [email, mettreEmail] = useState('');
  const [password, mettrePassword] = useState('');

  const { login, loading } = useContext(AuthContext);

  if (loading) {
    return <Loading />;
  }

  return (
      <View style={styles.container}>

        <Image
          style={styles.mainLogo}
          source={require('../raw/logo.png')}
        />

        <FormInput
            labelName="Email"
            value={email}
            autoCapitalize="none"
            onChangeText={(emailUtilisateur) => mettreEmail(emailUtilisateur)}
        />
        <FormInput
            labelName="Mot de passe"
            value={password}
            secureTextEntry={true}
            onChangeText={(passUtilisateur) => mettrePassword(passUtilisateur)}
        />
        <FormButton
            title="Se connecter"
            modeValue="contained"
            labelStyle={styles.loginButtonLabel}
            onPress={() => login(email, password)}
        />
        <FormButton
            title="Inscrivez-vous"
            modeValue="text"
            uppercase={false}
            labelStyle={styles.navButtonText}
            onPress={() => navigation.navigate('Signup')}
        />
      </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#f5f5f5',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  mainLogo: {
    width: 250,
    height: 100,
  },
  titleText: {
    fontSize: 24,
    marginBottom: 10,
  },
  loginButtonLabel: {
    fontSize: 22,
  },
  navButtonText: {
    fontSize: 16,
  },
});
