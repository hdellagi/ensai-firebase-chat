import React, { useContext, useState } from 'react';
import { StyleSheet, View } from 'react-native';
import { IconButton, Title } from 'react-native-paper';

import FormButton from '../components/FormButton';
import FormInput from '../components/FormInput';

export default function SignupScreen({ navigation }) {
  const [nomAffichage, setDisplayName] = useState('');
  const [email, mettreEmail] = useState('');
  const [password, mettrePassword] = useState('');

  return (
      <View style={styles.container}>
        <Title style={styles.titleText}>Creer un compte!</Title>
        <FormInput
            labelName="Display Name"
            value={nomAffichage}
            autoCapitalize="none"
            onChangeText={(userNomAffichage) => setDisplayName(userNomAffichage)}
        />
        <FormInput
            labelName="Email"
            value={email}
            autoCapitalize="none"
            onChangeText={(emailUtilisateur) => mettreEmail(emailUtilisateur)}
        />
        <FormInput
            labelName="Password"
            value={password}
            secureTextEntry={true}
            onChangeText={(passUtilisateur) => mettrePassword(passUtilisateur)}
        />
        <FormButton
            title="Signup"
            modeValue="contained"
            labelStyle={styles.loginButtonLabel}
            onPress={() => {
              // TODO
            }}
        />
        <IconButton
            icon="keyboard-backspace"
            size={30}
            style={styles.navButton}
            color="#5b3a60"
            onPress={() => navigation.goBack()}
        />
      </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#f5f5f5',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  titleText: {
    fontSize: 24,
    marginBottom: 10,
  },
  loginButtonLabel: {
    fontSize: 22,
  },
  navButtonText: {
    fontSize: 18,
  },
  navButton: {
    marginTop: 10,
  },
});
