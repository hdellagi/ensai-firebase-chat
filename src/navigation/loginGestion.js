import React, { createContext, useState } from 'react';

import { firebase } from '../firebase';
import { kitty } from '../chatServer';

export const AuthContext = createContext({});

export const loginGestion = ({ children }) => {
  const [user, metterUtilisateur] = useState(null);
  const [loading, setLoading] = useState(false);

  return (
      <AuthContext.Provider
          value={{
            user,
            metterUtilisateur,
            loading,
            setLoading,
            login: async (email, password) => {
              setLoading(true);

              const result = await kitty.startSession({
                username: email,
                authParams: {
                  password: password,
                },
              });

              setLoading(false);

            },
            /*
            register: async (nomAffichage, email, password) => {
              setLoading(true);

              try {
                await firebase
                .auth()
                .createUserWithEmailAndPassword(email, password)
                .then((credential) => {
                  credential.user
                  .updateProfile({ nomAffichage: nomAffichage })
                  .then(async () => {
                    const result = await kitty.startSession({
                      username: email,
                      authParams: {
                        password: password,
                      },
                    });

                  });
                });
              } catch (e) {
                console.log(e);
              }

              setLoading(false);
            },
            */
            logout: async () => {
              try {
                await kitty.endSession();
              } catch (e) {
                console.error(e);
              }
            },
          }}
      >
        {children}
      </AuthContext.Provider>
  );
};
