import { createStackNavigator } from '@react-navigation/stack';
import React from 'react';
import { IconButton } from 'react-native-paper';

import BrowseChannelsScreen from '../screens/BrowseChannelsScreen';
import msgScreen from '../screens/msgScreen';
import CreateChannelScreen from '../screens/CreateChannelScreen';
import accueilScreen from '../screens/accueilScreen';

const ChatStack = createStackNavigator();
const ModalStack = createStackNavigator();

export default function AccueilStack() {
  return (
      <ModalStack.Navigator mode="modal" headerMode="none">
        <ModalStack.Screen name="ChatApp" component={ChatComponent} />
        <ModalStack.Screen name="CreateChannel" component={CreateChannelScreen} />
      </ModalStack.Navigator>
  );
}

function ChatComponent() {
  return (
      <ChatStack.Navigator
          screenOptions={{
            headerStyle: {
              backgroundColor: '#5b3a70',
            },
            headerTintColor: '#ffffff',
            headerTitleStyle: {
              fontSize: 22,
            },
          }}
      >
        <ChatStack.Screen
            name="Home"
            component={accueilScreen}
            options={({ navigation }) => ({
              headerRight: () => (
                  <IconButton
                      icon="plus"
                      size={28}
                      color="#ffffff"
                      onPress={() => navigation.navigate('BrowseChannels')}
                  />
              ),
            })}
        />
        <ChatStack.Screen
            name="BrowseChannels"
            component={BrowseChannelsScreen}
            options={({ navigation }) => ({
              headerRight: () => (
                  <IconButton
                      icon="plus"
                      size={28}
                      color="#ffffff"
                      onPress={() => navigation.navigate('CreateChannel')}
                  />
              ),
            })}
        />
        <ChatStack.Screen
            name="Chat"
            component={msgScreen}
            options={({ route }) => ({
              title: route.params.channel.name,
            })}
        />
      </ChatStack.Navigator>
  );
}
