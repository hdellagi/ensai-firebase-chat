import { createStackNavigator } from '@react-navigation/stack';
import React from 'react';

import SeConnecterScreen from '../screens/SeConnecterScreen';
import SignupScreen from '../screens/SignupScreen';

const Stack = createStackNavigator();

export default function loginStack() {
  return (
      <Stack.Navigator initialRouteName="Login" headerMode="none">
        <Stack.Screen name="Login" component={SeConnecterScreen} />
        <Stack.Screen name="Signup" component={SignupScreen} />
      </Stack.Navigator>
  );
}
