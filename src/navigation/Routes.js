import { NavigationContainer } from '@react-navigation/native';
import React, { useContext, useEffect, useState } from 'react';



import { AuthContext } from './loginGestion';
import loginStack from './loginStack';
import AccueilStack from './AccueilStack';

import { kitty } from '../chatServer';
import Loading from '../components/Loading';

export default function Routes() {
  const { user, metterUtilisateur } = useContext(AuthContext);
  const [loading, setLoading] = useState(true);
  const [initializing, setInitializing] = useState(true);

  useEffect(() => {
    return kitty.onCurrentUserChanged((currentUser) => {
      metterUtilisateur(currentUser);

      if (initializing) {
        setInitializing(false);
      }

      setLoading(false);
    });
  }, [initializing, metterUtilisateur]);

  if (loading) {
    return <Loading />;
  }

  return (
      <NavigationContainer>
        {user ? <AccueilStack /> : <loginStack />}
      </NavigationContainer>
  );
}

